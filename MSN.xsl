<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : MSN.xsl
    Created on : December 6, 2012, 5:27 PM
    Author     : Jessinta Lilaponggampanart
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:variable name="FriendName" select="//Message[1]/From/User/@FriendlyName"/>
    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <style type="text/css">
                body {font-family: Verdana, arial, sans-serif;}         
            </style>
            <head>
                <title>Kanda Saikaew' s MSN Log</title>
            </head>
           
           
            <span style="color:red">[Conversation started at
                                  
                <xsl:value-of select="Log/Message[1]/@Date"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="Log/Message[1]/@Time"/>
                <xsl:text>] </xsl:text>
                                  
            </span>
        
            <table border="0">
                <xsl:for-each select="Log/Message">
                    <tr>
                        <td>[
                            <xsl:value-of select="@Date"/>
                              <xsl:text> </xsl:text>
                            <xsl:value-of select="@Time"/>]
                        </td>
                        <xsl:choose>
                            <xsl:when test="contains(From/User/@FriendlyName, $FriendName)">
                                <td>
                                    <span style="color: #FFAA00"> 
                                        <xsl:value-of select="From/User/@FriendlyName"/>
                                    </span>
                                </td>
                                <td>:</td>
                                <td>
                                    <span style="color: #FFAA00"> 
                                        <xsl:value-of select="Text"/>
                                    </span>
                                </td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td>
                                    <span style="color: #24913C"> 
                                        <xsl:value-of select="From/User/@FriendlyName"/>
                                    </span>
                                </td>
                                <td>:</td>
                                <td>
                                    <span style="color: #24913C"> 
                                        <xsl:value-of select="Text"/>
                                    </span>
                                </td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tr>    
                </xsl:for-each>
            </table>
                   
            <body>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
